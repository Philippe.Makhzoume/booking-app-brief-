import Express from 'express'

const app = Express();

export default async (controller, isLoggedIn, upload) => {
    app.get('/', (req, res, next) => res.send("Ok from Doctors datas"))

    // CREATE
    app.post("/doctors/new", isLoggedIn, upload.single('image'), async (req, res, next) => {
        const author_id = req.user.sub
        try {
            const {
                name,
                specialization,
                phoneNbr,
                email,
                experience,
                price
            } = req.query;
            const image = req.file && req.file.filename
            const result = await controller.createDoctor({
                name,
                specialization,
                phoneNbr,
                email,
                experience,
                price,
                image,
                author_id
            });
            res.json({
                success: true,
                result
            });
        } catch (e) {
            next(e);
        }
    });

    // DELETE
    app.get("/doctors/delete/:id", isLoggedIn, async (req, res, next) => {
        const author_id = req.user.sub
        try {
            const {
                id
            } = req.params;
            const result = await controller.deleteDoctor({
                id,
                author_id
            });
            res.json({
                success: true,
                result
            })
        } catch (e) {
            next(e)
        }
    })

    // UPDATE
    app.post("/doctors/update/:id", isLoggedIn, upload.single('image'), async (req, res, next) => {
        const author_id = req.user.sub
        try {
            const {
                id
            } = req.params;
            const {
                name,
                specialization,
                phoneNbr,
                email,
                experience,
                price
            } = req.query;
            const image = req.file && req.file.filename
            const result = await controller.updateDoctor(id, {
                name,
                specialization,
                phoneNbr,
                email,
                experience,
                price,
                author_id,
                image
            });
            res.json({
                success: true,
                result
            });
        } catch (e) {
            next(e);
        }
    });

    // READ
    app.get('/doctors/get/:id', async (req, res, next) => {
        try {
            const {
                id
            } = req.params
            const result = await controller.getDoctor(id)
            res.json({
                success: true,
                result: result
            })
        } catch (e) {
            next(e)
        }
    })

    // LIST
    app.get("/doctors/list", async (req, res, next) => {
        try {
            const {
                order,
                desc
            } = req.query;
            const result = await controller.getDoctorsList({
                order,
                desc
            });
            res.json({
                success: true,
                result: result
            });
        } catch (e) {
            next(e);
        }
    });

    return app;
}