import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const nowForSQLite = () => new Date().toISOString().replace('T',' ').replace('Z','');

const joinSQLStatementKeys = (keys, values, delimiter , keyValueSeparator='=') => {
  return keys
    .map(propName => {
      const value = values[propName];
      if (value !== null && typeof value !== "undefined") {
        return SQL``.append(propName).append(keyValueSeparator).append(SQL`${value}`);
      }
      return false;
    })
    .filter(Boolean)
    .reduce((prev, curr) => prev.append(delimiter).append(curr));
};

const initializeDatabase = async () => {
  // Force to use the current dir path
  const path = require('path');
  const dbPath = path.resolve(__dirname, './db.sqlite');
  const db = await sqlite.open(dbPath);
  
  const createDoctor = async (props) => {
    if (!props || 
      !props.name || 
      !props.specialization || 
      !props.phoneNbr || 
      !props.email || 
      !props.experience ||
      !props.price ||
      !props.author_id) {
      throw new Error(`you must provide a name, a specialization, a phone number, an email, experience, price and an author_id`);
    }

    const { name, specialization, phoneNbr, email, experience, price, author_id, image } = props;
    const date = nowForSQLite();
    try {
      const result = await db.run(
        SQL`INSERT INTO doctors (name, specialization, phoneNbr, email, experience, price, date, image, author_id) VALUES 
        (${name}, ${specialization}, ${phoneNbr}, ${email}, ${experience}, ${price}, ${date}, ${image}, ${author_id})`
      );
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+ e.message)
    }
  }

  const deleteDoctor = async (props) => {
    const { id, author_id } = props
    try {
      const result = await db.run(
        SQL`DELETE FROM doctors WHERE doctor_id = ${id} AND author_id = ${author_id}`
      );
      if (result.stmt.changes === 0) {
        throw new Error(`doctor "${id}" does not exist or wrong author_id`);
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the doctor "${id}": `+e.message)
    }
  }
  
  const updateDoctor = async (doctor_id, props) => {
    if (
      (!props || !(props.name ||  props.specialization || props.phoneNbr || props.email || props.experience || props.price || props.image), !props.author_id)
    ) {
      throw new Error(
        `you must provide a name, or specialization, or phoneNbr, or email, or experience or price or image, and an author_id`
      );
    }
    try {
      const previousProps = await getDoctor(doctor_id)
      const newProps = {...previousProps, ...props }
      const statement = SQL`UPDATE doctors SET `
        .append(
          joinSQLStatementKeys(
            ["name", "specialization", "phoneNbr", "email", "experience", "price", "image"],
            newProps,
            ", "
          )
        )
        .append(SQL` WHERE `)
        .append(
          joinSQLStatementKeys(
            ["doctor_id", "author_id"],
            { doctor_id:doctor_id, author_id:props.author_id },
            " AND "
          )
        );
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the doctor ${doctor_id}: ` + e.message);
    }
  };

  const getDoctor = async id => {
    try {
      const doctorsList = await db.all(
        SQL`SELECT doctor_id AS id, name, specialization, phoneNbr, email, experience, price, image, author_id FROM doctors WHERE doctor_id = ${id}`
      );
      const doctor = doctorsList[0];
      if(!doctor){
        throw new Error(`doctor ${id} not found`)
      }
      return doctor;
    }catch(e){
      throw new Error(`couldn't get the doctor ${id}: `+e.message)
    }
  }
  
  const getDoctorsList = async props => {
    const { orderBy, author_id, desc, limit, start } = props;
    const orderProperty = /name|specialization|phoneNbr|email|experience|price|date|doctor_id/.test(orderBy)
      ? orderBy
      : "doctor_id";
    const startingId = start 
      ? start // if start is provided, use that
      : orderProperty === "doctor_id" // otherwise, if we're order by `doctor_id`:
      ? 0 // default `startingId` is 0 
      : orderProperty === "date" // otherwise, if we're ordering by `date`
      ? "1970-01-01 00:00:00.000" // default property is an old date
      : "a"; // otherwise, default property is "a" (for `name` and `email`)
    try {
      const statement = SQL`SELECT doctor_id AS id, name, specialization, phoneNbr, email, experience, price, date, image, author_id FROM doctors WHERE ${orderProperty} > ${startingId}`;
      if (author_id) {
        statement.append(SQL` AND author_id = ${author_id}`);
      }
      statement.append(
        desc
          ? SQL` ORDER BY ${orderProperty} DESC`
          : SQL` ORDER BY ${orderProperty} ASC`
      );
      statement.append(SQL` LIMIT ${limit || 100}`);
      const rows = await db.all(statement);
      return rows;
    } catch (e) {
      throw new Error(`couldn't retrieve doctors: ` + e.message);
    }
  };

  const createUserIfNotExists = async props => {
    const { auth0_sub, nickname } = props;
    const answer = await db.get(
      SQL`SELECT user_id FROM users WHERE auth0_sub = ${auth0_sub}`
    );
    if (!answer) {
      await createUser(props)
      return {...props, firstTime:true } // if the user didn't exist, make that clear somehow
    }
    return props;
  };

  /**
   * Creates a user
   * @param {Object} props an object containing the properties `auth0_sub` and `nickname`.  
   */
  const createUser = async props => {
    const { auth0_sub, nickname } = props;
    const result = await db.run(SQL`INSERT INTO users (auth0_sub, nickname) VALUES (${auth0_sub},${nickname});`);
    return result.stmt.lastID;
  }

  const controller = {
    createUserIfNotExists,
    createUser,
    createDoctor,
    deleteDoctor,
    updateDoctor,
    getDoctor,
    getDoctorsList
  }

  return controller
}


export default initializeDatabase
