import React, { Component } from "react";
import { withRouter, Route, Switch, Link } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { pause, makeRequestUrl } from "./utils.js";
import "./App.css";
/* AUTH */
import * as auth0Client from "./auth";
import SecuredRoute from './SecuredRoute';
/* COMPONENTS */
import Navbar from "./Components/Navbar/Navbar"
import DoctorList from "./Components/Doctors/DoctorList";
import Doctor from "./Components/Doctors/Doctor";

const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);

class App extends Component {
  state = {
    doctors_list: [],
    error_message: "",
    name: "",
    specialization: "",
    phoneNbr: "",
    experience: "",
    price: "",
    isLoading: false,
    checkingSession: true
  };
  async componentDidMount() {
    this.getDoctorsList();
    if (this.props.location.pathname === "/callback") {
      this.setState({ checkingSession: false });
      return;
    }
    try {
      await auth0Client.silentAuth();
      await this.getPersonalPageData(); // get the data from our server
    } catch (err) {
      if (err.error !== "login_required") {
        console.log(err.error);
      }
    }
    this.setState({ checkingSession: false });
  }
  getDoctor = async id => {
    // check if we already have the doctor
    const previous_doctor = this.state.doctors_list.find(
      doctor => doctor.id === id
    );
    if (previous_doctor) {
      return; // do nothing, no need to reload a doctor we already have
    }
    try {
      const url = makeUrl(`doctors/get/${id}`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of doctors
        const doctor = answer.result;
        const doctors_list = [...this.state.doctors_list, doctor];
        this.setState({ doctors_list });
        toast(`doctor loaded`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  deleteDoctor = async id => {
    try {
      const url = makeUrl(`doctors/delete/${id}`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const doctors_list = this.state.doctors_list.filter(
          doctor => doctor.id !== id
        );
        this.setState({ doctors_list });
        toast(`doctor deleted`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  updateDoctor = async (id, props) => {
    try {
      if (!props || !(props.name || props.specialization || props.phoneNbr  || props.email || props.experience || props.price)) {
        throw new Error(
          `you need at least name or specialization, or e-mail, experience and price properties to update doctor information`
        );
      }
      const url = makeUrl(`doctors/update/${id}`, {
        name: props.name,
        specialization: props.specialization,
        phoneNbr: props.phoneNbr,
        email: props.email,
        experience: props.experience,
        price: props.price
      });

      let body = null;

      if(props.image)
      {
        body = new FormData();
        body.append(`image`, props.image)
      }

      const response = await fetch(url, {
        method:'POST', 
        body,
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // we update the user, to reproduce the database changes:
        const doctors_list = this.state.doctors_list.map(doctor => {
          // if this is the doctor we need to change, update it. This will apply to exactly
          // one doctor
          if (doctor.id === id) {
            const new_doctor = {
              id: doctor.id,
              name: props.name || doctor.name,
              specialization: props.specialization || doctor.specialization,
              phoneNbr: props.phoneNbr || doctor.phoneNbr,
              email: props.email || doctor.email,
              experience: props.experience || doctor.experience,
              price: props.price || doctor.price
            };
            toast(`doctor "${new_doctor.name}" updated`);
            return new_doctor;
          }
          // otherwise, don't change the doctor at all
          else {
            return doctor;
          }
        });
        this.setState({ doctors_list });
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };
  createDoctor = async props => {
    try {
      if (!props || !(props.name && props.specialization && props.phoneNbr && props.email && props.experience && props.price)) {
        throw new Error(
          `you need both name, specialization, email, experience, price properties to create a doctor`
        );
      }
      const { name, specialization, phoneNbr, email, experience, price, image } = props;
      const url = makeUrl(`doctors/new`, {
        name,
        specialization,
        phoneNbr,
        email,
        experience,
        price
      });
      
      let body = null;
      if(image)
      {
        body = new FormData();
        body.append(`image`, image)
      }

      const response = await fetch(url, {
        method:'POST', 
        body,
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the doctor that was created in the database, locally
        const id = answer.result;
        const doctor = { name, specialization, phoneNbr, email, experience, price, id };
        const doctors_list = [...this.state.doctors_list, doctor];
        this.setState({ doctors_list });
        toast(`doctor "${name}" added`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  getDoctorsList = async order => {
    this.setState({ isLoading: true });
    try {
      const url = makeUrl(`doctors/list`, { order });
      const response = await fetch(url);
      await pause();
      const answer = await response.json();
      if (answer.success) {
        const doctors_list = answer.result;
        this.setState({ doctors_list, isLoading: false });
        toast("doctors loaded");
      } else {
        this.setState({ error_message: answer.message, isLoading: false });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading: false });
      toast.error(err.message);
    }
  };
  getPersonalPageData = async () => {
    try {
      const url = makeUrl(`mypage`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        const user = answer.result;
        this.setState({ user });
        if (user.firstTime) {
          toast(`welcome ${user.nickname}! We hope you'll like it here'`);
        }
        toast(`hello ${user.nickname}'`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(
          `error message received from the server: ${answer.message}`
        );
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };
  onSubmit = evt => {
    // stop the form from submitting:
    evt.preventDefault();
    // extract name and specialization, phoneNbr, email, experience, price from state
    const { name, specialization, phoneNbr, email, experience, price } = this.state;
    // get the files
    const image = evt.target.doctor_image_input.files[0]
    // Add the doctor from name, specialization, phoneNbr, email, experience, price and image
    this.createDoctor({ name, specialization, phoneNbr, email, experience, price, image });
    // empty name, specialization, phoneNbr, email, experience, price so the text input fields are reset
    this.setState({ name: "", specialization: "", phoneNbr: "", email: "", experience: "", price: "" });
    this.props.history.push("/");
  };
  renderUser() {
    const isLoggedIn = auth0Client.isAuthenticated();
    if (isLoggedIn) {
      // user is logged in
      return this.renderUserLoggedIn();
    } else {
      return this.renderUserLoggedOut();
    }
  }
  renderUserLoggedOut() {
    return <button onClick={auth0Client.signIn}>Sign In</button>;
  }
  renderUserLoggedIn() {
    const nick = auth0Client.getProfile().name;
    const user_doctors = this.state.user.result.map(doctorFromUser =>
      this.state.doctors_list.find(
        doctorFromMain => doctorFromMain.id === doctorFromUser.id
      )
    );
    return (
      <div>
        Hello, {nick}!{" "}
        <button
          onClick={() => {
            auth0Client.signOut();
            this.setState({});
          }}
        >
          logout
        </button>
        <div>
          <DoctorList doctors_list={user_doctors} />
        </div>
      </div>
    );
  }
  renderHomePage = () => {
    const { doctors_list } = this.state;
    return (
    <div>
      <div>
        <h2> THIS IS A MAGIC CLINIC ! </h2>
        <a> Our Doctors... </a>
      </div>
      <div>
        <DoctorList doctors_list={doctors_list} />
      </div>
    </div>
    );
  };
  renderDoctorPage = ({ match }) => {
    const id = match.params.id;
    // find the doctor:
    // eslint-disable-next-line eqeqeq
    const doctor = this.state.doctors_list.find(doctor => doctor.id == id);
    // we use double equality because our ids are numbers, and the id provided by the router is a string
    if (!doctor) {
      return <div>{id} not found</div>;
    }
    return (
      <Doctor
        id={doctor.id}
        name={doctor.name}
        specialization={doctor.specialization}
        phoneNbr={doctor.phoneNbr}
        email={doctor.email}
        experience={doctor.experience}
        price={doctor.price}
        author_id={doctor.author_id}
        image={doctor.image}
        updateDoctor={this.updateDoctor}
        deleteDoctor={this.deleteDoctor}
      />
    );
  };
  renderProfilePage = () => {
    if (this.state.checkingSession) {
      return <p>validating session...</p>;
    }
    return (
      <div>
        <p>profile page</p>
        {this.renderUser()}
      </div>
    );
  };
  renderCreateForm = () => {
    return (
      <form className="third" onSubmit={this.onSubmit}>
        <input
          type="text"
          placeholder="name"
          onChange={evt => this.setState({ name: evt.target.value })}
          value={this.state.name}
        />
        <input
          type="text"
          placeholder="specialization"
          onChange={evt => this.setState({ specialization: evt.target.value })}
          value={this.state.specialization}
        />
        <input
          type="text"
          placeholder="phoneNbr"
          onChange={evt => this.setState({ phoneNbr: evt.target.value })}
          value={this.state.phoneNbr}
        />
        <input
          type="text"
          placeholder="email"
          onChange={evt => this.setState({ email: evt.target.value })}
          value={this.state.email}
        />
        <input
          type="text"
          placeholder="experience"
          onChange={evt => this.setState({ experience: evt.target.value })}
          value={this.state.experience}
        />
        <input
          type="text"
          placeholder="price"
          onChange={evt => this.setState({ price: evt.target.value })}
          value={this.state.price}
        />
        <input
          type="file"
          name="doctor_image_input"
        />
        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" className="button" />
        </div>
      </form>
    );
  };
  renderContent() {
    if (this.state.isLoading) {
      return <p>loading...</p>;
    }
    return (
      <Switch>
        <Route path="/" exact render={this.renderHomePage} />
        <Route path="/doctor/:id" render={this.renderDoctorPage} />
        <Route path="/profile" render={this.renderProfilePage} />
        <SecuredRoute path="/create" render={this.renderCreateForm} />
        <Route path="/callback" render={this.handleAuthentication} />
        <Route render={() => <div>not found!</div>} />
      </Switch>
    );
  }
  isLogging = false;
  login = async () => {
    if (this.isLogging === true) { 
      return;
    }
    this.isLogging = true;
    try {
      await auth0Client.handleAuthentication();
      await this.getPersonalPageData(); // get the data from our server
      this.props.history.push("/profile");
    } catch (err) {
      this.isLogging = false;
      toast.error(`error from the server: ${err.message}`);
    }
  };
  handleAuthentication = () => {
    this.login();
    return <p>wait...</p>;
  };
  render() {
    return (
      <div className="App">
        <Navbar />
        {this.renderContent()}
        <ToastContainer />
      </div>
    );
  }
}

export default withRouter(App);
