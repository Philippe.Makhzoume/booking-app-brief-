import React from 'react';
import * as auth0Client from "../../auth";
import Booking from "./Schedules/Booking"
import Schedule from "./Schedules/Schedule"
import './Doctor.css'
import '../Style/w3.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPhone, faHandHoldingUsd, faMedkit, faEnvelope, faUser, faSuitcase } from '@fortawesome/free-solid-svg-icons'

library.add(faPhone, faHandHoldingUsd, faMedkit, faEnvelope, faUser, faSuitcase)


const IconName = ({json}) => {
  return (
    <div>
    {json.map(x =>
    <p key={json.indexOf(x)}>
      <FontAwesomeIcon
      icon = {x[0]}
      color = "black"
      size = "sm"
      className = "w3-margin-right"
      />
      {Object.values(x[1])[0]}
    </p>
    )}
    </div>
  );
}

export default class Doctor extends React.Component
{
    state = {
      editMode:false
    }
    toggleEditMode = () => {
      const editMode = !this.state.editMode
      this.setState({editMode})
    }
    renderEditMode(){
        const {
          name,
          specialization,
          phoneNbr,
          email,
          experience,
          price
        } = this.props;
        return(
        <form className="third" onSubmit={this.onSubmit} onReset={this.toggleEditMode}>
          <input
            type="text"
            placeholder="name"
            name="doctor_name_input"
            defaultValue={name}
          />
          <input
            type="text"
            placeholder="specialization"
            name="doctor_specialization_input"
            defaultValue={specialization}
          />
          <input
            type="text"
            placeholder="phoneNbr"
            name="doctor_phoneNbr_input"
            defaultValue={phoneNbr}
          />
          <input
            type="text"
            placeholder="email"
            name="doctor_email_input"
            defaultValue={email}
          />
          <input
            type="text"
            placeholder="experience"
            name="doctor_experience_input"
            defaultValue={experience}
          />
          <input
            type="text"
            placeholder="price"
            name="doctor_price_input"
            defaultValue={price}
          />
          <input
              type="file"
              name="doctor_image_input"
          />
          <div>
            <input type="submit" value="ok" />
            <input type="reset" value="cancel" className="button" />
          </div>
        </form>
        )
      }
      onSubmit = (evt) => {
        // stop the page from refreshing
        evt.preventDefault()
        // target the form
        const form = evt.target
        // extract the two inputs from the form
        const doctor_name_input = form.doctor_name_input 
        const doctor_specialization_input = form.doctor_specialization_input 
        const doctor_phoneNbr_input = form.doctor_phoneNbr_input 
        const doctor_email_input = form.doctor_email_input 
        const doctor_experience_input = form.doctor_experience_input 
        const doctor_price_input = form.doctor_price_input 
		    const doctor_image_input = form.doctor_image_input;
        // extract the values
        const name = doctor_name_input.value
        const specialization = doctor_specialization_input.value
        const phoneNbr = doctor_phoneNbr_input.value
        const email = doctor_email_input.value
        const experience = doctor_experience_input.value
        const price = doctor_price_input.value
		    const image = doctor_image_input.files[0];
        // get the id and the update function from the props
        const { id, updateDoctor } = this.props
        // run the update doctor function
        updateDoctor(id,{ name, specialization, phoneNbr, email, experience, price, image })
        // toggle back view mode
        this.toggleEditMode()
      }
    
      renderViewMode() {
        const {
          id,
          name,
          specialization,
          phoneNbr,
          email,
          experience,
          price,
          author_id,
          deleteDoctor,
          image
        } = this.props;
        const isLoggedIn = auth0Client.isAuthenticated();
        const current_logged_in_user_id = isLoggedIn && auth0Client.getProfile().sub
        const is_author = author_id === current_logged_in_user_id;
        const iconNameData = [
          ["user", {name} ],
          ["medkit", {specialization} ],
          ["phone", {phoneNbr}],
          ["envelope", {email}],
          ["suitcase", {experience}],
          ["hand-holding-usd", {price}]
        ]
        return (
        <div className="box">
          <br/><br/>
          <div className="box-left w3-card-4">
              <div className="box-edit">
                { is_author && isLoggedIn ?
                <div>
                    <button onClick={this.toggleEditMode} className="success btn-edit ">
                    Edit Doctor Profile
                    </button>
                    <button onClick={() => deleteDoctor(id)} className="warning btn-right">
                    x
                    </button>
                </div>
                : false
                }
              </div>
              { image && <img src={`//localhost:8080/images/${image}`} alt={`the avatar of ${name}`}/> }
              <div >
                <ul>
                <IconName json = {iconNameData} />
                </ul>
              </div>
          </div>
          <div className="box-right">
            <Booking />
            <Schedule />
          </div>
        </div>
        );
    }    
    render()
    {
      const { editMode } = this.state
      if(editMode){
        return this.renderEditMode()
      }
      else
      {
        return this.renderViewMode()
      }
    }
}
  