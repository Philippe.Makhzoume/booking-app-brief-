import React from "react";
import { Transition } from "react-spring"; // you can remove this from App.js
import { Link } from "react-router-dom";
import './DoctorList.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faMedkit,  faUser, } from '@fortawesome/free-solid-svg-icons'

library.add(faMedkit, faUser)

// Add a Filter Button if you have time
const DoctorList = ({ doctors_list }) => (
  <div className="row">
    <Transition
        items={doctors_list}
        keys={doctor =>
        doctor.id}
        from={{ transform: "translate3d(-100px,0,0)" }}
        enter={{ transform: "translate3d(0,0px,0)" }}
        leave={{ transform: "translate3d(-100px,0,0)" }}
        >
        { doctor => style => (
        <div style={style}>
          <div className="column">
              <div className="card">
                { doctor.image && <img className="img" src={`//localhost:8080/images/${doctor.image}`} alt={`the avatar of ${doctor.name}` }/> }
                <p>
                    <FontAwesomeIcon
                      icon = {faUser}
                      color = "black"
                      size = "sm"
                      className = "w3-margin-right"
                    />
                    {doctor.name}
                </p>
                <p>
                    <FontAwesomeIcon
                      icon = {faMedkit}
                      color = "black"
                      size = "sm"
                      className = "w3-margin-right"
                    />
                    {doctor.specialization}
                </p>
                <Link to={"/doctor/" + doctor.id}>
                <button type="button"> More Details</button> 
                </Link>
              </div>
          </div>
        </div>
        )}
    </Transition>
  </div>
);

export default DoctorList
