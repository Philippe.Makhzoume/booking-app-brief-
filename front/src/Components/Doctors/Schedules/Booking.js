import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import getDay from "date-fns/getDay";

// import { Link } from "react-router-dom";
import './Booking.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faBed, faCalendarAlt, faClock } from '@fortawesome/free-solid-svg-icons'

library.add(faBed, faCalendarAlt, faClock)

// This is because i'm lazy (And apparently it took me more time and more line of codes)
const ComboBox = ({data}) => {
  return (
    <section>
      <FontAwesomeIcon
      icon =  {data.iconName}
      color = "black"
      size = "sm"
      className = "w3-margin-right"
      />
      <select id={data.idName} name={data.idName}>
      { data.value.map(x => 
       <option key={data.value.indexOf(x)} value={toString(data.value.indexOf(x))}>{x}</option>
      )}
      </select>
    </section>
  );
};

export default class Booking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: null,
      hour: null,
      room: null
    };
    this.handleChange = this.handleChange.bind(this);
  }
 
  handleChange(date) {
    this.setState({
      startDate: date
    });
  }

  isWeekday = date => {
    const day = getDay(date);
    return day !== 0 && day !== 6;
  };

  // Check Availability 
  // Validate Booking if Logged in
    // > Update DB
  
  // Add Edit Option if logged as Admin < ?

  // Mount
  // onSubmit
    render() {
      const roomData = {
        'idName': "room",
        'iconName': faBed,
        'value': [
          ["Room 1"],
          ["Room 2"],
          ["Room 3"],
          ["Room 4"],
          ["Room 5"]
        ]
      };

      const timeData = {
        'idName': "time",
        'iconName': faClock,
        'value': [
          ["10:00"],
          ["11:00"],
          ["12:00"],
          ["13:00"],
          ["14:00"],
          ["15:00"],
        ]
      };
      return (
        <main className="box-top">
        <h2> Magnificent Booking System </h2>
        <div>
          <form className="div-fill-form">
            <section>
              <FontAwesomeIcon
              icon = {faCalendarAlt}
              color = "black"
              size = "sm"
              className = "w3-margin-right"
              />
            <DatePicker
              selected={this.state.startDate}
              onChange={this.handleChange}
              filterDate={this.isWeekday}
              shouldCloseOnSelect={false}
              minDate={new Date()}
              showDisabledMonthNavigation
              placeholderText="Pick a date"
            />
            </section>

            <ComboBox data = {timeData} />
            <ComboBox data = {roomData} />

            <button className="book-button" type="button"> Book </button> 
          </form>
        </div>
      </main>
      );
    }
}
