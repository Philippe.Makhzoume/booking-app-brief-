import React, { Component } from 'react';
import ResponsiveMenu from 'react-responsive-navbar';
import { Link } from "react-router-dom";
import IfAuthenticated from '../../IfAuthenticated';
import './Navbar.css';

// TODO UNDERSTAND RESPONSIVE CODE...

class Navbar extends Component {
    render() {
      return (
        <ResponsiveMenu
          menuOpenButton={<div />}
          menuCloseButton={<div />}
          changeMenuOn="600px"
          largeMenuClassName="topnav"
          smallMenuClassName="myTopnav responsive"
          menu={
            <div id="myTopnav">
                <Link to="/">Home</Link> /
                <Link to="/profile">Profile</Link> /
                <Link to="/schedule">Schedules</Link> 
                <IfAuthenticated>
                    / <Link to="/create">Create</Link>
                </IfAuthenticated>
            </div>
          }
        />
      );
    }
}

export default Navbar;
